This library lets you do some fancy things by chaining Either (Atlassian Fugue Either)

Just realize, that can also be this generic (Check TestOperation testcase)
```
    Either<AnError, Double> either = new Operation()
                    .add(() -> firstOKOperation())
                    .add(str -> secondNGOperation())
                    .add((str, number) -> thirdOKOperation())
                    .add((str, number, boo) -> firstOKOperation())
                    .map((str, number, boo, str2) -> new Double(number));
```

It will only work with Java 8 for now, given we utilize Function, Consumer, Supplier from JDK 8