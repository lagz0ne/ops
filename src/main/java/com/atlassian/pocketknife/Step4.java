package com.atlassian.pocketknife;

import com.atlassian.fugue.Either;
import com.atlassian.pocketknife.functions.Function4;
import com.atlassian.pocketknife.functions.Function3;
import com.atlassian.pocketknife.ops.Step;

public class Step4<E1, E2, E3, E4, E> extends Step<E, E4>
{
    private final Either<E, E1> either1;
    private final Either<E, E2> either2;
    private final Either<E, E3> either3;
    private final Either<E, E4> either4;

    public Step4(Either<E, E1> either1,
                 Either<E, E2> either2,
                 Either<E, E3> either3,
                 Function3<E1, E2, E3, Either<E, E4>> function3)
    {
        this.either1 = either1;
        this.either2 = either2;
        this.either3 = either3;
        this.either4 = either1.flatMap(value1 ->
                either2.flatMap(value2 ->
                        either3.flatMap(value3 -> function3.apply(value1, value2, value3))));
    }

    public <E5> Step5<E1, E2, E3, E4, E5, E> then(Function4<E1, E2, E3, E4, Either<E, E5>> function4)
    {
        return new Step5<>(either1, either2, either3, either4, function4);
    }

    public <RESULT> Either<E, RESULT> yield(Function4<E1, E2, E3, E4, RESULT> callback)
    {
        return either1.flatMap(value1 ->
                either2.flatMap(value2 ->
                        either3.flatMap(value3 ->
                                either4.map(value4 -> callback.apply(value1, value2, value3, value4)))));
    }

    @Override
    public Either<E, E4> toEither()
    {
        return either4;
    }
}
