package com.atlassian.pocketknife;

import com.atlassian.fugue.Either;
import com.atlassian.pocketknife.functions.Function4;
import com.atlassian.pocketknife.functions.Function5;
import com.atlassian.pocketknife.ops.Step;

public class Step5<E1, E2, E3, E4, E5, E> extends Step<E, E5>
{
    private final Either<E, E1> either1;
    private final Either<E, E2> either2;
    private final Either<E, E3> either3;
    private final Either<E, E4> either4;
    private final Either<E, E5> either5;

    public Step5(Either<E, E1> either1,
                 Either<E, E2> either2,
                 Either<E, E3> either3,
                 Either<E, E4> either4,
                 Function4<E1, E2, E3, E4, Either<E, E5>> function4)
    {
        this.either1 = either1;
        this.either2 = either2;
        this.either3 = either3;
        this.either4 = either4;
        this.either5 = either1.flatMap(value1 ->
                either2.flatMap(value2 ->
                        either3.flatMap(value3 ->
                                either4.flatMap(value4 -> function4.apply(value1, value2, value3, value4)))));
    }

    public <Z> Either<E, Z> yield(Function5<E1, E2, E3, E4, E5, Z> function5)
    {
        return either1.flatMap(value1 ->
                either2.flatMap(value2 ->
                        either3.flatMap(value3 ->
                                either4.flatMap(value4 ->
                                        either5.map(value5 -> function5.apply(value1, value2, value3, value4, value5))))));
    }

    @Override
    public Either<E, E5> toEither()
    {
        return either5;
    }
}