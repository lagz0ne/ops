package com.atlassian.pocketknife.ops;

import com.atlassian.fugue.Either;

public abstract class Step<ERROR, VALUE>
{
    public abstract Either<ERROR, VALUE> toEither();
}
