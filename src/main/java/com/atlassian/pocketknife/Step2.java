package com.atlassian.pocketknife;

import com.atlassian.fugue.Either;
import com.atlassian.pocketknife.functions.Function2;
import com.atlassian.pocketknife.ops.Step;

import java.util.function.BiFunction;
import java.util.function.Function;

public class Step2<E1, E2, E> extends Step<E, E2>
{

    private final Either<E, E1> either1;
    private final Either<E, E2> either2;

    public Step2(Either<E, E1> either1, Function<? super E1, Either<E, E2>> function2)
    {
        this.either1 = either1;
        this.either2 = either1.flatMap(function2::apply);
    }

    public <E3> Step3<E1, E2, E3, E> then(BiFunction<E1, E2, Either<E, E3>> function3)
    {
        return new Step3<>(either1, either2, function3);
    }

    public <Z> Either<E, Z> yield(Function2<E1, E2, Z> function2)
    {
        return either1.flatMap(value1 -> either2.map(value2 -> function2.apply(value1, value2)));
    }

    @Override
    public Either<E, E2> toEither()
    {
        return either2;
    }
}
