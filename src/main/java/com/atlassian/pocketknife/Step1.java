package com.atlassian.pocketknife;

import com.atlassian.fugue.Either;
import com.atlassian.pocketknife.functions.Function1;
import com.atlassian.pocketknife.ops.Step;

import java.util.function.Function;

public class Step1<E1, E> extends Step<E, E1>
{

    private final Either<E, E1> either1;

    public Step1(Either<E, E1> either1)
    {
        this.either1 = either1;
    }

    public <E2> Step2<E1, E2, E> then(Function<? super E1, Either<E, E2>> function2)
    {
        return new Step2<>(either1, function2);
    }

    public <Z> Either<E, Z> yield(Function1<E1, Z> function1)
    {
        return either1.map(e1 -> function1.apply(e1));
    }

    public Either<E, E1> toEither()
    {
        return either1;
    }
}
