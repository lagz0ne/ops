package com.atlassian.pocketknife;

import com.atlassian.fugue.Either;

public class Steps
{
    public static <E1, E> Step1<E1, E> begin(Either<E, E1> either)
    {
        return new Step1<>(either);
    }
}
