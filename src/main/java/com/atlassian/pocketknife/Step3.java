package com.atlassian.pocketknife;

import com.atlassian.fugue.Either;
import com.atlassian.pocketknife.functions.Function3;
import com.atlassian.pocketknife.ops.Step;

import java.util.function.BiFunction;

public class Step3<E1, E2, E3, E> extends Step<E, E3>
{

    private final Either<E, E1> either1;
    private final Either<E, E2> either2;
    private final Either<E, E3> either3;

    public Step3(Either<E, E1> either1,
                 Either<E, E2> either2,
                 BiFunction<? super E1, ? super E2, Either<E, E3>> function3)
    {
        this.either1 = either1;
        this.either2 = either2;

        this.either3 = either1.flatMap(value1 -> either2.flatMap(value2 -> function3.apply(value1, value2)));
    }

    public <E4> Step4<E1, E2, E3, E4, E> then(Function3<E1, E2, E3, Either<E, E4>> function4)
    {
        return new Step4<>(either1, either2, either3, function4);
    }

    public <Z> Either<E, Z> yield(Function3<E1, E2, E3, Z> function3)
    {
        return either1.flatMap(value1 -> either2.flatMap(value2 -> either3.map(value3 -> function3.apply(value1, value2, value3))));
    }

    @Override
    public Either<E, E3> toEither()
    {
        return either3;
    }
}
