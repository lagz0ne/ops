package com.atlassian.pocketknife.test;

public class AnError
{

    static class FIRST extends AnError
    {
        static FIRST ERROR = new FIRST();

        private FIRST() {}
    }

    static class SECOND extends AnError
    {
        static SECOND ERROR = new SECOND();

        private SECOND() {}
    }

    static class THIRD extends AnError
    {
        static THIRD ERROR = new THIRD();

        private THIRD() {}
    }

    static class FORTH extends AnError
    {
        static FORTH ERROR = new FORTH();

        private FORTH() {}
    }

    static class FIFTH extends AnError
    {
        static FIFTH ERROR = new FIFTH();

        private FIFTH() {}
    }
}
