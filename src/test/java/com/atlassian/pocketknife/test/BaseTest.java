package com.atlassian.pocketknife.test;

import com.atlassian.fugue.Either;

public class BaseTest
{

    protected Either<AnError, String> firstOKOperation(String str)
    {
        return Either.right(str);
    }

    protected Either<AnError, String> firstNGOperation(String str)
    {
        return Either.left(AnError.FIRST.ERROR);
    }

    protected Either<AnError, Long> secondNGOperation(Long number)
    {
        return Either.left(AnError.SECOND.ERROR);
    }

    protected Either<AnError, Long> secondOKOperation(Long number)
    {
        return Either.right(number);
    }

    protected Either<AnError, Boolean> thirdOKOperation(Boolean value)
    {
        return Either.right(value ? false : true);
    }
    protected Either<AnError, Boolean> thirdNGOperation(Boolean value)
    {
        return Either.left(AnError.THIRD.ERROR);
    }

    protected Either<AnError, String> fourthOKOperation(String value)
    {
        return Either.right(value.toLowerCase());
    }
    protected Either<AnError, String> fourthNGOperation(String value)
    {
        return Either.left(AnError.FORTH.ERROR);
    }

    protected Either<AnError, Long> fifthOKOperation(Long value)
    {
        return Either.right(value / 2);
    }
    protected Either<AnError, Long> fifthNGOperation(Long value)
    {
        return Either.left(AnError.FIFTH.ERROR);
    }

}
