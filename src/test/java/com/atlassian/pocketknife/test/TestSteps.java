package com.atlassian.pocketknife.test;

import com.atlassian.fugue.Either;
import com.atlassian.pocketknife.Steps;
import org.junit.Assert;
import org.junit.Test;

public class TestSteps extends BaseTest
{

    private static String STRING = "123456";
    private static String STRING_UPPERED = "QWERTY";
    private static String STRING_LOWERED = "qwerty";
    private static Long   LONG   = 123456L;
    private static Long   LONGLONG   = 123456123456L;

    @Test
    public void test_1_step_success()
    {

        Either<AnError, Long> stepped = Steps
                .begin(firstOKOperation(STRING))
                .yield(value1 -> new Long(value1));

        Assert.assertTrue("Should be success", stepped.isRight());
        Assert.assertTrue("Should be 123456", stepped.right().get().equals(LONG));
    }

    @Test
    public void test_1_step_failure()
    {
        Either<AnError, Long> stepped = Steps
                .begin(firstNGOperation(STRING))
                .yield(value1 -> new Long(value1));

        Assert.assertTrue("Should be failure", stepped.isLeft());
        Assert.assertTrue("Should be FirstError", stepped.left().get() == AnError.FIRST.ERROR);
    }

    @Test
    public void test_2_step_success()
    {
        Either<AnError, Long> stepped = Steps.begin(firstOKOperation(STRING))
                .then(str -> secondOKOperation(LONG))
                .yield((str, integer) -> new Long(str + integer));

        Assert.assertTrue("Should be success", stepped.isRight());
        Assert.assertTrue("Should be LONGLONG", stepped.right().get().equals(LONGLONG));
    }

    @Test
    public void test_2_step_failure()
    {
        Either<AnError, Long> stepped = Steps.begin(firstOKOperation(STRING))
                .then(str -> secondNGOperation(LONG))
                .yield((str, integer) -> new Long(str + integer));

        Assert.assertTrue("Should be failure", stepped.isLeft());
        Assert.assertTrue("Should be SecondError", stepped.left().get() == AnError.SECOND.ERROR);
    }

    @Test
    public void test_3_step_success()
    {
        Either<AnError, String> stepped = Steps.begin(firstOKOperation(STRING))
                .then(str -> secondOKOperation(LONG))
                .then((string, aLong) -> thirdOKOperation(true))
                .yield((string, integer, boo) -> string + integer + boo);

        Assert.assertTrue("Should be success", stepped.isRight());
        Assert.assertTrue("Should be LONGLONG", stepped.right().get().equals(STRING + LONG + false));
    }

    @Test
    public void test_3_step_failure()
    {
        Either<AnError, String> stepped = Steps.begin(firstOKOperation(STRING))
                .then(str -> secondOKOperation(LONG))
                .then((string, aLong) -> thirdNGOperation(true))
                .yield((string, integer, boo) -> string + integer + boo);

        Assert.assertTrue("Should be failure", stepped.isLeft());
        Assert.assertTrue("Should be LONGLONG", stepped.left().get() == AnError.THIRD.ERROR);
    }

    @Test
    public void test_4_step_success()
    {
        Either<AnError, String> stepped = Steps.begin(firstOKOperation(STRING))
                .then(str -> secondOKOperation(LONG))
                .then((string, aLong) -> thirdOKOperation(true))
                .then((string, aLong, aBoolean) -> fourthOKOperation(STRING_UPPERED))
                .yield((string, integer, boo, string2) -> string + integer + boo + string2);

        Assert.assertTrue("Should be success", stepped.isRight());
        Assert.assertTrue("Should be LONGLONG", stepped.right().get().equals(STRING + LONG + false + STRING_LOWERED));
    }

    @Test
    public void test_4_step_failure()
    {
        Either<AnError, String> stepped = Steps.begin(firstOKOperation(STRING))
                .then(str -> secondOKOperation(LONG))
                .then((string, aLong) -> thirdOKOperation(true))
                .then((string, aLong, aBoolean) -> fourthNGOperation(STRING_UPPERED))
                .yield((string, integer, boo, string2) -> string + integer + boo + string2);

        Assert.assertTrue("Should be failure", stepped.isLeft());
        Assert.assertTrue("Should be LONGLONG", stepped.left().get() == AnError.FORTH.ERROR);
    }

    @Test
    public void test_5_step_success()
    {
        Either<AnError, String> stepped = Steps.begin(firstOKOperation(STRING))
                .then(str -> secondOKOperation(LONG))
                .then((string, aLong) -> thirdOKOperation(true))
                .then((string, aLong, aBoolean) -> fourthOKOperation(STRING_UPPERED))
                .then((string, aLong, aBoolean, string2) -> fifthOKOperation(LONG))
                .yield((string, integer, boo, string2, ll) -> string + integer + boo + string2 + ll);

        Assert.assertTrue("Should be success", stepped.isRight());
        Assert.assertTrue("Should be LONGLONG", stepped.right().get().equals(STRING + LONG + false + STRING_LOWERED + (LONG / 2)));
    }

    @Test
    public void test_5_step_failure()
    {
        Either<AnError, String> stepped = Steps.begin(firstOKOperation(STRING))
                .then(str -> secondOKOperation(LONG))
                .then((string, aLong) -> thirdOKOperation(true))
                .then((string, aLong, aBoolean) -> fourthOKOperation(STRING_UPPERED))
                .then((string, aLong, aBoolean, string2) -> fifthNGOperation(LONG))
                .yield((string, integer, boo, string2, ll) -> string + integer + boo + string2 + ll);

        Assert.assertTrue("Should be failure", stepped.isLeft());
        Assert.assertTrue("Should be LONGLONG", stepped.left().get() == AnError.FIFTH.ERROR);
    }

}
